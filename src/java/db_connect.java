
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author lenovopc
 */
public class db_connect {

    Connection con;
    Statement st;
    ResultSet rst;

    public db_connect() {

    }

    public Connection setConnection() {
        try {
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/kayitformu", "postgres",
					"Merve.aslan123");
            System.out.println("Bağlantı yapıldı");
        } catch (Exception e) {

            System.out.println("Bağlantı yapılamadı");
        }
        return con;

    }

    public ResultSet getResult(String sql, Connection con) {
        this.con = con;
        try {
            st = con.createStatement();
            rst = st.executeQuery(sql);
        } catch (Exception e) {
        }
        return rst;

    }
    
    public static void main(String[] args) {
        db_connect db = new db_connect();
        db.setConnection();
    }
}
